El archivo implementa la tecnica de programacion genetica (GP) para resolver el problema
de encontrar una expresion aritmetica que se acerque a cierto numero, en nuestro caso 50.

El algoritmo consiste en generar aleatoriamente una poblacion de programas que en cada 
generacion seran puestos a prueba y a los que mejor les vaya seran seleccionados para
transmitir su informacion genetica. Los nuevos individuos son generados mediante
cross overs y mutaciones.

Para correr el programa basta con ir a la carpeta y hacer python GP.py en la consola.

Dentro del main del programa se pueden realizar cambios a los parametros del objeto, 
como la probabilida de mutacion, tamaño de la poblacion, generaciones, etc.
