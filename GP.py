import random

class Node:
    def __init__(self, value, right = None, left = None, parent = None):
        self.value = value
        self.right = right
        self.left = left
        self.parent = parent
        self.isLeaf = False


    def size(self):
        if self.isLeaf:
            return 1
        else:
            return 1 + self.left.size() + self.right.size()


    def height(self):
        if self.isLeaf:
            return 0
        else:
            try:
                return 1 + max(self.left.height(), self.right.height())
            except RuntimeError as re:
                return 1000

    def getRoot(self):
        root = None
        done = False
        startNode = self
        while not done:
            if startNode.parent:
                root = startNode.parent
                startNode = root
            else:
                done = True
        return root


    def randomNode(self):
        r = random.randint(1,self.size())
        if r == 1:
            return self
        elif r >= 2 and r <= self.left.size():
            return self.left.randomNode()
        else:
            return self.right.randomNode()



    def crossOver(self, subtree):
        if self.height() > 10:
            return self
        r = random.randint(1,self.size())
        if r == 1:
            return self.getRoot()

        else:

            if r >= 2 and r <= self.left.size():
                return self.left.crossOver(subtree)
            else:
                return self.right.crossOver(subtree)


    def grow(self, height, operations, terminals):
        rand = random.randint(0,1)
        if not self.isLeaf:
            if height == 0:
                self.left = Node(random.choice(terminals), parent = self)
                self.left.isLeaf = True
                self.right = Node(random.choice(terminals), parent = self)
                self.right.isLeaf = True

            else:
                if rand > 0.5:
                    self.left = Node(random.choice(operations), parent = self)
                else:
                    self.left = Node(random.choice(terminals), parent = self)
                    self.left.isLeaf = True
                if rand > 0.5:
                    self.right = Node(random.choice(operations), parent = self)
                else:
                    self.right = Node(random.choice(terminals), parent = self)
                    self.right.isLeaf = True

                self.left.grow(height - 1, operations , terminals)
                self.right.grow(height - 1, operations , terminals)

    def printTree(self, level = 0):
        print '__' * level + str(self.value)
        if self.isLeaf:
            pass
        else:
            self.left.printTree(level + 1)
            self.right.printTree(level + 1)

    def evaluate(self):
        if self.isLeaf:
            return self.value

        else:

            left = self.left.evaluate()
            right = self.right.evaluate()
            if self.value == '+':
                return left + right
            elif self.value == '*':
                return left * right
            elif self.value == '-':
                return left - right
            else:
                # Protective Division
                if right == 0:
                    return 1
                else:
                    return left/right

    def mutate(self, mutProb, operations, terminals):
        rand = random.randint(0,1)
        if self.isLeaf:
            if rand > mutProb:
                self.value = random.choice(terminals)
        else:
            if rand > mutProb:
                self.value = random.choice(operations)
            self.left.mutate(mutProb, terminals, operations)
            self.right.mutate(mutProb, terminals, operations)

class Tree:
    def __init__(self):
        self.operations = ['+', '-', '*', '/']
        self.terminals = list(range(0,10))
        self.root = Node(random.choice(self.operations))

    def grow(self, height):
        self.root.grow(height, self.operations, self.terminals)

    def printTree(self):
        self.root.printTree()

    def execute(self):
        return self.root.evaluate()

    def randomNode(self):
        return self.root.randomNode()

    def crossOver(self, subtree, maxHeight):
        self.root.crossOver(subtree)
        return self.root

    def mutate(self, mutProb):
        def mutate(self, mutationProb):
            self.root.mutate(mutationProb, self.operations, self.terminals)



class GP:
    def __init__(self, popSize, maxGenerations, maxHeight, crossProb, mutProb):
        self.popSize = popSize
        self.population = []
        self.fittest = []
        for i in range(popSize):
            height = random.randint(1,maxHeight)
            tree = Tree()
            tree.grow(height)
            self.population.append(tree)
        self.maxGen = maxGenerations
        self.crossProb = crossProb
        self.mutProb = 0.9
        self.maxHeight = maxHeight

    def testAndSelect(self, goal):
        measures = []
        self.fittest = []
        for tree in self.population:
            fitness = abs(goal - tree.execute())
            measures.append((tree,fitness))
        measures.sort(key=lambda x: x[1])
        measures = measures[0:len(measures)/2]
        for x in measures:
            self.fittest.append(x[0])
        return self.fittest[0]

    def matingTime(self):
        self.population = []

        for tree in self.fittest:
            self.population.append(tree)

        delta = self.popSize - len(self.population)

        while len(self.population) != self.popSize:
            papa, mama = random.sample(self.fittest,2)
            crossOverPoint = papa.randomNode()
            hijo = Tree()
            hijo.root = mama.crossOver(crossOverPoint, self.maxHeight)

            if hijo.root.height() <= self.maxHeight:
                self.population.append(hijo)
            else:
                pass

    def mutate(self):
        for tree in self.population:
            tree.mutate(self.mutProb)



if __name__ == '__main__':
    flag = True

    test = GP(10,10,8,0.9,0.1)
    generacion = 0
    goal = 50
    print('La meta es llegar a {}'.format(goal))

    while generacion < test.maxGen:
        try:
            print('Generacion {}'.format(generacion))
            generacion+=1
            bestOne = test.testAndSelect(goal)
            if bestOne.execute() == goal:
                break
            test.matingTime()
            test.mutate()
        except RuntimeError as re:
            print('Maximo de recursion excedido!, repetir experimento porfavor.')
            flag = False
            break


    print("El mejor individuo que se encontro fue:")
    print(bestOne.printTree())
    print("El resultado de su evaluacion es: {}".format(bestOne.execute()))
